import Page from '../lib/index.spec';
import { should } from 'chai';

should();

let page: any;

describe('Validar funcionalidade básica da homepage de webinarios', function () {
  this.timeout(30000);

  before(async function () {
    page = new (Page as any)();
    page.visit("http://localhost:3000/webinario/znt-homologacao-001---editado2");
  });

  after(async function () {
    await page.quit();
  });

  it('confirmar dados do webinario', async function () {
    const titulo = await page.tituloDoWebinario();
    const data = await page.dataDoWebinario();
    const hora = await page.horaDoWebinario();

    titulo.should.equal("Znt homologacao 001 - editado2");
    data.should.equal("09 de março");
    hora.should.equal("20h00");
  });
});
