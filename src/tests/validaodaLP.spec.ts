import { Builder, By, Key, ThenableWebDriver, until } from 'selenium-webdriver'
import assert from 'assert'

describe('Validação da LP', function () {
  this.timeout(30000)
  let driver: ThenableWebDriver;
  let vars: { [x: string]: { toString: () => string; }; }
  beforeEach(async function () {
    driver = new Builder().forBrowser('chrome').build()
    vars = {}
    // setWindowSize: definir a largura da página
    await driver.manage().window().setRect({ width: 1186, height: 1200 })
  })
  afterEach(async function () {
    await driver.quit();
  })
  it('Validação da LP', async function () {
    // open: Abrir a página
    await driver.get("http://localhost:3000/webinario/znt-homologacao-001---editado2")
    // waitForElementPresent: aguardar campo ser exibido na tela
    await driver.wait(until.elementLocated(By.xpath("//*[@data-testid=\'lblTituloWebinar\']")), 10000, "Título do webinario não encontrado")
    // assertText: garantir título correto do webinario
    assert(await driver.findElement(By.xpath("//*[@data-testid=\'lblTituloWebinar\']")).getText() == "Znt homologacao 001 - editado2")
    // assertText: garantir data correta do webinário
    assert(await driver.findElement(By.xpath("//*[@data-testid=\'boxDataWebinar\']/div/div[2]/p[@data-testid=\'lblDiaWebinar\']")).getText() == "09 de março")
    // assertText: garantir horário correto do webinário
    assert(await driver.findElement(By.xpath("//*[@data-testid=\'boxDataWebinar\']/div/div[2]/p[@data-testid=\'lblHoraWebinar\']")).getText() == "20h00")
    // click: Clicar no botão Garantir Meu Lugar no header
    await driver.findElement(By.xpath("//*[@data-testid=\'btnHeaderGarantirMeuLugar\']")).click()
    // executeScript: Obter posição do scroll
    vars["posicaoScroll"] = await driver.executeScript("return window.pageYOffset < 500")
    // assert: Verificar se scroll subiu após apertar em Garantir Meu Lugar
    assert(vars["posicaoScroll"].toString() == "true")
    // click: Clicar para preencher email no form
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-email\']")).click()
    // type: Preencher e-mail no form
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-email\']")).sendKeys("abc123@email123.com")
    // sendKeys: enviar key ENTER para avançar no formulário
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-email\']")).sendKeys(Key.ENTER)
    // click: Clicar no input de CRM
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-crm\']")).click()
    // type: Preencher CRM 149690
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-crm\']")).sendKeys("149690")
    // click: Clicar no select de CRM/UF
    await driver.findElement(By.xpath("//select[@data-testid=\'subscription-form-crm-state\']")).click()
    // select: Selecionar São Paulo
    {
      const dropdown = await driver.findElement(By.xpath("//select[@data-testid=\'subscription-form-crm-state\']"))
      await dropdown.findElement(By.xpath("//option[. = 'São Paulo']")).click()
    }
    // click: Clicar no botão de avançar
    await driver.findElement(By.xpath("//button[@data-testid=\'subscription-form-button-submit-crm\']")).click()
    // click: Clicar no input de nome pra ver se existe
    await driver.findElement(By.xpath("//input[@data-testid=\'edtNome\']")).click()
    // assertText: Verificar se nome está preenchido com Lorenzo
    {
      const value = await driver.findElement(By.xpath("//input[@data-testid=\'edtNome\']")).getAttribute("value")
      assert(value == "Lorenzo")
    }
    // click: Clicar no input sobrenome pra ver se existe
    await driver.findElement(By.xpath("//input[@data-testid=\'edtSobrenome\']")).click()
    // click: Clicar no input telefone
    await driver.findElement(By.xpath("//input[@data-testid=\'edtTelefone\']")).click()
    // type: Preencher telefone
    await driver.findElement(By.id("mobile_phone")).sendKeys("(11) 11111-1111")
    // click: Clicar no checkbox de termos de comunicação
    await driver.findElement(By.xpath("//*[@data-testid=\'checkTermosComunicacao\']")).click()
    // click: Clicar no checkbox de termos de uso
    await driver.findElement(By.xpath("//*[@data-testid=\'checkTermosUso\']")).click()
    // click: Clicar no checkbox de termos de privacidade
    await driver.findElement(By.xpath("//*[@data-testid=\'checkTermosPrivacidade\']")).click()
    // click: Clicar no link para os termos de uso
    await driver.findElement(By.linkText("termos de uso")).click()
    // assertChecked: Verificar se checkbox não foi desmarcado após clicar no link externo do termo de uso
    assert(await driver.findElement(By.xpath("//*[@data-testid=\'checkTermosUso\']/input")).isSelected())
    // click: Clicar no link para os termos de privacidade
    await driver.findElement(By.linkText("política de privacidade da plataforma")).click()
    // assertChecked: Verificar se checkbox não foi desmarcado após clicar no link externo do termo de privacidade
    assert(await driver.findElement(By.xpath("//*[@data-testid=\'checkTermosPrivacidade\']/input")).isSelected())
    // click: Clicar no botão de voltar no formulário
    await driver.findElement(By.xpath("//button[@data-testid=\'btnVoltar\']")).click()
    // click: Clicar para garantir que exibiu o input de CRM
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-crm\']")).click()
    // click: Clicar no botão de voltar no formulário
    await driver.findElement(By.xpath("//button[@data-testid=\'btnVoltar\']")).click()
    // click: Clicar no input de e-mail pra garantir que ele está sendo exibido
    await driver.findElement(By.xpath("//input[@data-testid=\'subscription-form-email\']")).click()
  })
})
