import Page from './base_page.spec';

Page.prototype.tituloDoWebinario = function (): Promise<string> {
  return this.find('lblTituloWebinar').getText() as Promise<string>;
}

Page.prototype.dataDoWebinario = function (): Promise<string> {
  const lblDiaWebinar = "/div/div[2]/p[@data-testid=\'lblDiaWebinar\']";
  return this.find('boxDataWebinar', lblDiaWebinar).getText() as Promise<string>;
}

Page.prototype.horaDoWebinario = function (): Promise<string> {
  const lblHoraWebinar = "/div/div[2]/p[@data-testid=\'lblHoraWebinar\']";
  return this.find('boxDataWebinar', lblHoraWebinar).getText() as Promise<string>;
}

Page.prototype.scrollParaBaixo = function (): Promise<void> {
  return this.executeScript("window.scrollBy(0,500)") as Promise<void>;
}

Page.prototype.btnHeaderGarantirMeuLugar = function (): Promise<void> {
  return this.click('btnHeaderGarantirMeuLugar') as Promise<void>;
}

Page.prototype.posicaoDoScroll = async function (): Promise<boolean> {
  const resultado = await (this.executeScript("return window.pageYOffset < 500") as Promise<boolean>);
  return resultado;
}

export default Page;
