import { By } from "selenium-webdriver";

const prefix = 'subscription-';

/**
 * Etapa de validação de e-mail
 */
export const btnValidateEmail = By.xpath(
  `//*[@data-testid='${prefix}form-button-submit-email']`
);
export const elEmail = By.xpath(
  `//*[@data-testid='${prefix}form-email']`
);
export const elEmailMessageError = By.xpath(
  `//*[@data-testid='${prefix}form-email']/../div`
);
export const elCrm = By.xpath(
  `//*[@data-testid='${prefix}form-crm']`
);
export const elCrmMessageError = By.xpath(
  `//*[@data-testid='${prefix}form-crm']/../div`
);
export const elCrmState = By.xpath(
  `//*[@data-testid='${prefix}form-crm-state']`
);
export const elCrmStateMessageError = By.xpath(
  `//*[@data-testid='${prefix}form-crm-state']/../div`
);
