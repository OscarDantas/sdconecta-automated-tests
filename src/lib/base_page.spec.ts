import { Builder, By, ThenableWebDriver, until, WebElementPromise, Capabilities } from 'selenium-webdriver'
import { Options } from 'selenium-webdriver/chrome';


export default function (this: any) {

  const options = new Options();
  // options.setPageLoadStrategy('normal');
  // options.addArguments('no-sandbox');

  options.windowSize({ width: 1186, height: 700 });
  options.headless(); // função para rodar o teste sem abrir a janela do navegor (sem UI)
  options.addArguments('remote-debugging-port=9222'); // função para conseguirmos visualizar o teste abrindo no navegor manualmente: chrome://inspect

  this.driver = new Builder()
    .forBrowser('chrome')
    // .usingServer('http://10.90.69.38:4444/')
    // .withCapabilities(Capabilities.chrome())
    .setChromeOptions(options)
    .build();

  var driver: ThenableWebDriver = this.driver;

  this.visit = function (url: string) {
    return driver.get(url) as Promise<void>;
  }

  this.quit = function (): Promise<void> {
    return driver.quit();
  }

  this.find = function (el: string, path?: string) {
    driver.wait(until.elementLocated(By.xpath(`//*[@data-testid=\'${el}\']${path || ''}`)), 10000);
    return driver.findElement(By.xpath(`//*[@data-testid=\'${el}\']${path || ''}`)) as WebElementPromise;
  }

  this.sendKeys = function (el: string, key: string, childPath?: string): Promise<void> {
    return this.find(el, childPath).sendKeys(key);
  }

  this.click = function (el: string, childPath?: string): Promise<void> {
    return this.find(el, childPath).click() as Promise<void>;
  }

  this.selectDropdownValue = function (el: string, value: string, childPath?: string): Promise<void> {
    const dropdown = this.find(el, childPath);
    return dropdown.findElement(By.xpath(`//option[. = '${value}']`)).click();
  }

  this.executeScript = function (script: string): Promise<any> {
    return driver.executeScript(script) as Promise<any>;
  }

}
