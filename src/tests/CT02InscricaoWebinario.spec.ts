import { Builder, until, ThenableWebDriver } from "selenium-webdriver";
import { should } from "chai";
import assert from "assert";
import { generateCode } from "../utils";
import * as webinarSubscription from "../pagesObjects/webinarSubscriptionPage";
should();

const SLUG_WEBINARIO = "znt-homologacao-001---editado";
const NOME_WEBINARIO = "Znt homologacao 001 - editado";
const DATA_EVENTO = "09 de março";
const HORARIO_EVENTO = "20h00";

describe("Webinario inscrições", function () {
  let driver: ThenableWebDriver;
  this.timeout(60000);

  beforeEach(async function () {
    driver = new Builder().forBrowser("chrome").build();
    await driver.manage().window().setRect({ width: 1200, height: 800 });
  });

  afterEach(async function () {
    await driver.quit();
  });

  it("Validar dados essenciais do webinário na landpage", async function () {
    // Abrir a página
    await driver.get(`${process.env.URL_SITE}/${SLUG_WEBINARIO}`);

  });

  it("Novo inscrição de usuário no webinario", async function () {
    // Acessando tela de inscrição no webinario
    await driver.get(`${process.env.URL_SITE}/${SLUG_WEBINARIO}`);

    // Validando campos da primeira etapa da inscrição
    await driver.wait(
      until.elementLocated(webinarSubscription.btnValidateEmail),
      50000
    );
    await driver.findElement(webinarSubscription.btnValidateEmail).click();

    // Validando se apareceu mensagem de error "Campo obrigatório"
    assert(await driver.findElement(webinarSubscription.elEmailMessageError));

    // Inseriando e-mail invalido
    await driver
      .findElement(webinarSubscription.elEmail)
      .sendKeys("E-mail inválido");

    // Validando se apareceu mensagem de error "E-mail inválido"
    assert(
      (await driver
        .findElement(webinarSubscription.elEmailMessageError)
        .getText()) === "E-mail inválido"
    );

    // Inseriando e-mail válido
    await driver.findElement(webinarSubscription.elEmail).clear();
    await driver
      .findElement(webinarSubscription.elEmail)
      .sendKeys(`oscar${generateCode()}@teste.com`);

    // Submetendo formulario
    await driver.findElement(webinarSubscription.btnValidateEmail).click();
  });
});
