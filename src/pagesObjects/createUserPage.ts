import { By } from "selenium-webdriver";

/**
 * Etapa de validação de e-mail e código de convite
 */
export const btnValidateEmail = By.xpath(
  "//*[@data-testid='create-user-validate-email']"
);
export const elEmailInput = By.xpath(
  "//*[@data-testid='create-user-ds-login']"
);
export const elEmailMessageError = By.xpath(
  "//*[@data-testid='create-user-ds-login-control']/div[2]"
);

/**
 * Etapa de validação de CRM
 */
export const btnValidateCRM = By.xpath(
  "//*[@data-testid='create-user-validate-crm']"
);
export const elCrmInput = By.xpath(
  "//*[@data-testid='create-user-crm']"
);
export const elCrmMessageError = By.xpath(
  "//*[@data-testid='create-user-crm-control']/div[2]"
);
export const elCrmStateSearchInput = By.xpath(
  "//*[@data-testid='create-user-crm-state']/nz-select-top-control/nz-select-search/input"
);
export const elCrmStateMessageError = By.xpath(
  "//*[@data-testid='create-user-crm-state-control']/div[2]"
);
export const elCrmStateOptionItem = By.xpath(
  "//*[@class='cdk-overlay-pane crm-select-item-option-content']/nz-option-container/div/cdk-virtual-scroll-viewport/div/nz-option-item"
);

/**
 * Etapa dados pessoais
 */
export const btnCreate = By.xpath(
  "//*[@data-testid='create-user-create']"
);
export const elName = By.xpath(
  "//*[@data-testid='create-user-name']"
);
export const elNameMessageError = By.xpath(
  "//*[@data-testid='create-user-name-control']/div[2]"
);
export const elSurname = By.xpath(
  "//*[@data-testid='create-user-surname']"
);
export const elSurnameMessageError = By.xpath(
  "//*[@data-testid='create-user-surname-control']/div[2]"
);
export const elSpecialty = By.xpath(
  "//*[@data-testid='create-user-specialty']"
);
export const elSpecialtyMessageError = By.xpath(
  "//*[@data-testid='create-user-specialty-control']/div[2]"
);
export const elMobilePhone = By.xpath(
  "//*[@data-testid='create-user-mobile-phone']"
);
export const elMobilePhoneMessageError = By.xpath(
  "//*[@data-testid='create-user-mobile-phone-control']/div[2]"
);
export const elPassword = By.xpath(
  "//*[@data-testid='create-user-password']"
);
export const elPasswordMessageError = By.xpath(
  "//*[@data-testid='create-user-password-control']/div[2]"
);
export const elPasswordConfirm = By.xpath(
  "//*[@data-testid='create-user-password-confirm']"
);
export const elPasswordConfirmMessageError = By.xpath(
  "//*[@data-testid='create-user-password-confirm-control']/div[2]"
);
export const elTermsOfWhasappAccepted = By.xpath(
  "//*[@data-testid='create-user-terms-of-whatsapp-accepted']/span/input"
);
export const elTermsOfWhasappAcceptedMessageError = By.xpath(
  "//*[@data-testid='create-user-terms-of-whatsapp-accepted-control']/div[2]"
);
export const elUserTermAccepted = By.xpath(
  "//*[@data-testid='create-user-term-accepted']/span/input"
);
export const elUserTermAcceptedMessageError = By.xpath(
  "//*[@data-testid='create-user-term-accepted-control']/div[2]"
);
export const elPrivacityTermsAccept = By.xpath(
  "//*[@data-testid='create-user-privacity-terms-accept']/span/input"
);
export const elPrivacityTermsAcceptMessageError = By.xpath(
  "//*[@data-testid='create-user-privacity-terms-accept-control']/div[2]"
);