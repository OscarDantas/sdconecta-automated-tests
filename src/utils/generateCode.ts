export function generateCode(numberOfCharacters = 6): string {
  const characters ='abcdefghijklmnopqrstuvwxyz0123456789';
  // const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let code= '';
  const charactersLength = characters.length;

  for (let i = 0; i < numberOfCharacters; i++) {
    code += characters.charAt(Math.floor(Math.random() * charactersLength));
  }

  return code;
}