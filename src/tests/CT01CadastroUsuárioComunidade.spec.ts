import { Builder, until, ThenableWebDriver } from 'selenium-webdriver'
import { should } from 'chai';
import assert from 'assert';
import { generateCode } from '../utils';
import * as createUserPage from '../pagesObjects/createUserPage';
import * as confirmEmailPage from '../pagesObjects/confirmEmailPage';
should();

describe('Cadastro de usuário na comunidade', function () {
  let driver: ThenableWebDriver;
  this.timeout(60000);

  beforeEach(async function () {
    driver = new Builder().forBrowser('chrome').build();
    await driver.manage().window().setRect({ width: 1200, height: 800 });
  });

  afterEach(async function  () {
    await driver.quit();
  });

  it('Novo usuário se cadastrando e criando vinculo com comunidade', async function () {
    // Abrindo o site SDConecta na tela de cadastro de usuário na comunidade Clube da Mama
    await driver.get(`${process.env.URL_SITE}clube-da-mama/create?invite=SDCM`);
    
    // Validando campos da primeira etapa do cadastro
    await driver.wait(until.elementLocated(createUserPage.btnValidateEmail), 50000);
    await driver.findElement(createUserPage.btnValidateEmail).click();
    assert(await driver.findElement(createUserPage.elEmailMessageError));
    await driver.findElement(createUserPage.elEmailInput).sendKeys("oscar");
    
    // Validando campo de e-mail na primeira etapa do cadastro
    await driver.findElement(createUserPage.btnValidateEmail).click();
    assert(await driver.findElement(createUserPage.elEmailMessageError));
    assert(await driver.findElement(createUserPage.elEmailMessageError).getText() === "O e-mail informado não é válido");
    
    // Preenchendo e-mail valido na primeira etapa do cadastro
    await driver.findElement(createUserPage.elEmailInput).clear();
    await driver.findElement(createUserPage.elEmailInput).sendKeys(`oscar${generateCode()}@teste.com`);
    
    // Submetendo formulário da primeira etapa do cadastro
    await driver.findElement(createUserPage.btnValidateEmail).click();

    // Validando campos da segunda etapa do cadastro
    await driver.wait(until.elementLocated(createUserPage.btnValidateCRM), 50000);
    await driver.findElement(createUserPage.btnValidateCRM).click();
    assert(await driver.findElement(createUserPage.elCrmMessageError));
    assert(await driver.findElement(createUserPage.elCrmStateMessageError));

    // Preenchendo campo da segunda etapa do cadastro
    await driver.findElement(createUserPage.elCrmInput).sendKeys('CRM invalido');
    await driver.findElement(createUserPage.elCrmStateSearchInput).sendKeys('São Paulo');
    await driver.findElement(createUserPage.elCrmStateOptionItem).click();
    
    // Validando campo CRM para somente número na segunda etapa do cadastro
    await driver.findElement(createUserPage.btnValidateCRM).click();
    assert(await driver.findElement(createUserPage.elCrmMessageError).getText() === "Apenas números");
    await driver.findElement(createUserPage.elCrmInput).clear();
    await driver.findElement(createUserPage.elCrmInput).sendKeys('149690');

    // Submetendo formulário da segunda etapa do cadastro
    await driver.findElement(createUserPage.btnValidateCRM).click();
    
    // Validando campos da terceira etapa do cadastro
    await driver.wait(until.elementLocated(createUserPage.btnCreate), 50000);
    await driver.findElement(createUserPage.btnCreate).click();

    if ((await driver.findElement(createUserPage.elName).getText()).trim()) {
      assert(await driver.findElement(createUserPage.elNameMessageError));
    }

    if ((await driver.findElement(createUserPage.elSurname).getText()).trim()) {
      assert(await driver.findElement(createUserPage.elSurnameMessageError));
    }

    assert(await driver.findElement(createUserPage.elMobilePhoneMessageError));
    assert(await driver.findElement(createUserPage.elPasswordMessageError));
    assert(await driver.findElement(createUserPage.elPasswordConfirmMessageError));
    assert(await driver.findElement(createUserPage.elTermsOfWhasappAcceptedMessageError));
    assert(await driver.findElement(createUserPage.elUserTermAccepted));
    assert(await driver.findElement(createUserPage.elPrivacityTermsAccept));

    // Preenchendo campo da terceira etapa do cadastro
    await driver.findElement(createUserPage.elMobilePhone).sendKeys('(11) 11111-1111');
    await driver.findElement(createUserPage.elPassword).sendKeys('123123123');
    await driver.findElement(createUserPage.elPasswordConfirm).sendKeys('Senha incorreta');
    await driver.findElement(createUserPage.elTermsOfWhasappAccepted).click();
    await driver.findElement(createUserPage.elUserTermAccepted).click();
    await driver.findElement(createUserPage.elPrivacityTermsAccept).click();

    // Validando campo confirmar senha na terceira etapa do cadastro
    await driver.findElement(createUserPage.btnCreate).click();
    assert(await driver.findElement(createUserPage.elPasswordConfirmMessageError).getText() === "A senha é inconsistente!");
    
    // Preenchendo correta em confirmar senha na segunda etapa do cadastro
    await driver.findElement(createUserPage.elPasswordConfirm).clear();
    await driver.findElement(createUserPage.elPasswordConfirm).sendKeys('123123123');
    await driver.findElement(createUserPage.btnCreate).click();

    // Confirmando tela de sucesso
    await driver.wait(until.elementLocated(confirmEmailPage.elMessage), 50000);
    assert(await driver.findElement(confirmEmailPage.elMessage).getText() === "Por favor, verifique seu e-mail.");
    assert(await driver.getCurrentUrl() === `${process.env.URL_SITE}information-confirm-email`);
  });
});